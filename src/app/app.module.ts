import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { AppRoutingModule } from './app-routing.module';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AppComponent } from './app.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PieChartComponent } from './charts/pie-chart/pie-chart.component';
import { BarChartComponent } from './charts/bar-chart/bar-chart.component';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LineChartComponent } from './charts/line-chart/line-chart.component';
import { GuagechartComponent } from './charts/guagechart/guagechart.component';
import { PatternDetectionDetailsComponent } from './charts/pattern-detection-details/pattern-detection-details.component';

import { ChartsService } from '../app/charts/charts.service';

@NgModule({
  declarations: [
    AppComponent,
    SideNavComponent,
    HeaderNavComponent,
    DashboardComponent,
    PieChartComponent,
    BarChartComponent,
    LineChartComponent,
    GuagechartComponent,
    PatternDetectionDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    Ng2GoogleChartsModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule
  ],
  providers: [ChartsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
