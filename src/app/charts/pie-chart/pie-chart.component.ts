import { Component, OnInit, Input } from '@angular/core';
import { ChartReadyEvent } from 'ng2-google-charts';
import { ChartsService } from '../charts.service';
@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {
  @Input () pieChartData;
  @Input() type;
  @Input() title;
  totalvalue = '';
  isChartLoaded = false;
  constructor(private chartsService:ChartsService) { }

  ngOnInit() {
  }

  ready(eve){
    let total = 0;
    // let temp_array = this.pieChartData.dataTable;
    // temp_array.shift();
    // temp_array.forEach(item =>{
    //    total+=  item[item.length -1]
    // })
    this.chartsService.knowledgeBaseData().then(data=>{
      if(data['value'])
      this.totalvalue = data['value'];
    })
    this.isChartLoaded = true;
  }

}
