import { Component, OnInit , Input } from '@angular/core';
import { ChartsService } from '../charts.service';

@Component({
  selector: 'app-pattern-detection-details',
  templateUrl: './pattern-detection-details.component.html',
  styleUrls: ['./pattern-detection-details.component.scss']
})
export class PatternDetectionDetailsComponent implements OnInit {
  @Input() patternDetectionDatas;
  tableData = [];
  constructor(private chartsService:ChartsService) { }

  ngOnInit() {
    this.chartsService.pdrData().then(data=>{
      this.tableData = data['xAxis'][0]['details'];
    })
  }

  chartBarSelected(value){
    this.chartsService.pdrData().then(data=>{
      this.tableData = data['xAxis'][value]['details'];
    })
    
  }

}
