import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatternDetectionDetailsComponent } from './pattern-detection-details.component';

describe('PatternDetectionDetailsComponent', () => {
  let component: PatternDetectionDetailsComponent;
  let fixture: ComponentFixture<PatternDetectionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatternDetectionDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatternDetectionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
