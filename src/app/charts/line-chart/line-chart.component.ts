import { Component, OnInit, Input } from '@angular/core';
import { ChartReadyEvent } from 'ng2-google-charts';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {
  @Input () lineChartData;
  isChartLoaded = false;
  constructor() { }

  ngOnInit() {
  }
  ready(eve){
    this.isChartLoaded = true;
  }
}
