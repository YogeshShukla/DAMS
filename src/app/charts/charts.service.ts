import { Component, Input , Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';

@Injectable()
export class ChartsService{

    constructor(private http: Http) {
      
    }

    public convertPieChartData(resp,type){
      let Kb_array=[];
      Kb_array.push(['Title', type]);
      let res = type=='knowledgebase' ? resp.accuracyLevels : resp.networks;
      let k=0;
      res.forEach(item =>{
        for (var key in item){
          if (item.hasOwnProperty(key)) {
               Kb_array.push([key,item[key]])
          }
          k++;
        }
      })
      
      return (Kb_array)
    }
    public knowledgeBase() {
      return new Promise(resolve =>{
        this.http.get('assets/dataList/accuracy_graph.json')
        .subscribe(res => resolve(  this.convertPieChartData(res.json(),'knowledgebase') ) );
      })

     }
     
     public knowledgeBaseData() {
      return new Promise(resolve =>{
        this.http.get('assets/dataList/accuracy_graph.json')
        .subscribe(res => resolve(  res.json() ) );
      })

     }

     public network() {
      return new Promise(resolve =>{
        this.http.get('assets/dataList/networks.json')
        .subscribe(res => resolve(  this.convertPieChartData(res.json(),'network') ) );
      })

     }

     public pdr(){
      return new Promise(resolve =>{
        this.http.get('assets/dataList/pdr.json')
        .subscribe(res => resolve(  this.convertPdrData(res.json()) ) );
      })

     }

     public pdrData(){
      return new Promise(resolve =>{
        this.http.get('assets/dataList/pdr.json')
        .subscribe(res => resolve( res.json() ) );
      })
     }

     convertPdrData(resp){
      let barChart_array=[];
      barChart_array.push(['Title','User'])
      let res = resp.xAxis; 
      res.forEach(item =>{
            barChart_array.push([item.date,item.patternsDetected])
      })  
      return (barChart_array)
     }


     public gauge(){
      return new Promise(resolve =>{
        this.http.get('assets/dataList/speed_gauge.json')
        .subscribe(res => resolve(  this.convertGaugeData(res.json()) ) );
      })
     }

     convertGaugeData(resp){

        return resp;

     }

     public memory(){
      return new Promise(resolve =>{
        this.http.get('assets/dataList/Memory_infra.json')
        .subscribe(res => resolve(  this.convertMemoryData(res.json()) ) );
      })

     }

     convertMemoryData(resp){

      let memory_array = [];

      let dates = ['Dates'];
      dates = dates.concat(resp.types);
      memory_array.push(dates)

      resp.xAxis.forEach(item=>{
        let temp_array = [];
        temp_array.push(item.date);
        temp_array = temp_array.concat(item.units)
        memory_array.push(temp_array)
      })
        return memory_array;
     }

     public cpu(){
      return new Promise(resolve =>{
        this.http.get('assets/dataList/CPU_infra.json')
        .subscribe(res => resolve(  this.convertCpuData(res.json()) ) );
      })

     }

     convertCpuData(resp){
      let cpuData = [];
      let titles = ['Title'];
      titles = titles.concat(resp.types);
      cpuData.push(titles);

      resp.xAxis.forEach(item=>{
        let temp_array = [item.date,item.unit];
        cpuData.push(temp_array);
      })
        return cpuData;
     }

     public storage(){
      return new Promise(resolve =>{
        this.http.get('assets/dataList/Storage_infra.json')
        .subscribe(res => resolve(  this.convertStorageData(res.json()) ) );
      })
     }

     convertStorageData(resp){
      return this.convertCpuData(resp)
    }
     
}