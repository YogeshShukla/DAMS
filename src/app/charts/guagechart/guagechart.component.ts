import { Component, OnInit, Input } from '@angular/core';
import { ChartReadyEvent } from 'ng2-google-charts';

@Component({
  selector: 'app-guagechart',
  templateUrl: './guagechart.component.html',
  styleUrls: ['./guagechart.component.scss']
})
export class GuagechartComponent implements OnInit {
  @Input () guageChartData;
  @Input () guageChartTitle;
  isChartLoaded = false;
  constructor() { }

  ngOnInit() {
  }

  ready(eve){
    this.isChartLoaded = true;
  }

}
