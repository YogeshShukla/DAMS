import { Component, OnInit , ViewChild} from '@angular/core';
import { ChartsService } from '../charts/charts.service';
import { chartOptions } from '../charts/chart-options';
import {PatternDetectionDetailsComponent} from '../charts/pattern-detection-details/pattern-detection-details.component';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild(PatternDetectionDetailsComponent)
  private patternDetectionDetailsComponent:PatternDetectionDetailsComponent;
  constructor(private chartsService: ChartsService) { }

  selected = '';
  knowledgeBaseData = {};
  pieChartLoaded: boolean = false;
  memoryChartData = {};
  cpuChartData = {};
  storageChartData = {};
  pdrDate : string = '';
  guageChartData: any = {};
  guageChartData2: any = {};
  networkData: any = {};
  columnChartData:any =  {};
  patternDetectionData = {};

  knowledgeBaseLoaded = false;
  
  accounts = [
    { value: 'azure', viewValue: 'Azure' },
    { value: 'aws', viewValue: 'AWS' }
  ]
  public selectedMoments = [new Date(2018, 1, 12, 10, 30), new Date(2018, 3, 21, 20, 30)];
  ngOnInit() {

    let self = this;
    this.chartsService.knowledgeBase().then(data => {
      self.knowledgeBaseData = {
        chartType: 'PieChart',
        dataTable: data,
        options: chartOptions.knowledgeBase
      };
     self.knowledgeBaseLoaded = true;
    });

    this.chartsService.pdr().then(data => {
      self.patternDetectionData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: chartOptions.pdr
      };

      self.columnChartData = self.patternDetectionData;
    })

    this.chartsService.gauge().then(data=>{
      this.prepareGauge(data);
    })


    this.chartsService.memory().then(data=>{
      this.memoryChartData = {
        chartType: 'LineChart',
        dataTable: data,
        options: chartOptions.memory,
      };
    })

    this.chartsService.cpu().then(data=>{
      self.cpuChartData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: chartOptions.cpu
      };
    })

    this.chartsService.storage().then(data=>{
      self.storageChartData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: chartOptions.storage
      };
    })

    this.chartsService.network().then(data => {
      self.networkData = {
        chartType: 'PieChart',
        dataTable: data,
        options: chartOptions.network,
      };
    });

    this.chartsService.pdrData().then(data=>{
      this.pdrDate = data['xAxis'][0]['date'];
    })
    
  }

  selectedBar(value){
    this.chartsService.pdrData().then(data=>{
      this.pdrDate = data['xAxis'][value]['date'];
    })
    this.patternDetectionDetailsComponent.chartBarSelected(value);
  }

  prepareGauge(data){
    let self = this;
    let  kbValue= parseInt(data['knowledgeBase']['value']);
      let predictValue = parseInt(data['predictionAccuracy']['value']);

      let kbSteps = [];
      for(let k=parseInt(data['knowledgeBase']['min']);k<=parseInt(data['knowledgeBase']['max']);k+=parseInt(data['knowledgeBase']['stepIncrement'])){
        kbSteps.push(k);
      }

      let predictSteps = [];
      for(let j=parseInt(data['predictionAccuracy']['min']);j<=parseInt(data['predictionAccuracy']['max']);j+=parseInt(data['predictionAccuracy']['stepIncrement'])){
        predictSteps.push(j);
      }

      self.guageChartData = {
        chartType: 'Gauge',
        dataTable: [
          ['Label', 'Value'],
          ['', predictValue]
        ],
        options: chartOptions.gauge
      }

      self.guageChartData.options.min = parseInt(data['predictionAccuracy']['min'])
      self.guageChartData.options.max = parseInt(data['predictionAccuracy']['max'])
      self.guageChartData.options.majorTicks = predictSteps;
      

      self.guageChartData2 = {
        chartType: 'Gauge',
        dataTable: [
          ['Label', 'Value'],
          ['', kbValue]
        ],
         options: chartOptions.gauge
      }

      self.guageChartData2.options.min = parseInt(data['knowledgeBase']['min'])
      self.guageChartData2.options.max = parseInt(data['knowledgeBase']['max'])
      self.guageChartData2.options.majorTicks = kbSteps;
  }
 

}
